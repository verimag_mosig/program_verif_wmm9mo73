// TODO: only observe that it behaves from frama-c like in the pdf. 

#define MAXINT 2147483647

/*@ assigns *x;
  @ ensures \result == \old(*x) && *x == \result+1;
  @*/
int incr(int* x) {
  return (*x)++;
}


int main() {
  int x = MAXINT;
  int r = incr(&x);
  /*@ assert x > r; */
  if (x <= r) return 1/(r+1-x);
  return x;
}
