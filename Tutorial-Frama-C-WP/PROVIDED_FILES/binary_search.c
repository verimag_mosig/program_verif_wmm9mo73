/*@ requires
  @   \valid(t+(0..n-1)) &&
  @   \forall unsigned int k1, k2; k1 <= k2 < n ==> t[k1] <= t[k2];
  @ assigns \nothing;
  @ ensures
  @   (\result < n && t[\result] == v) ||
  @   (\result == n && \forall unsigned int k; k < n ==> t[k] != v);
  @*/
unsigned int binary_search(int* t, unsigned int n, int v) {
  // TODO: fix the implementation and prove it !
  unsigned int l=0, u=n-1;
  while (l <= u ) {
    unsigned int m = (l+u) / 2;
    if (t[m] < v) l = m + 1;
    else if (t[m] > v) u = m - 1;
    else return m;
  }
  return n;
}

////////////////////////
// test driver for binary_search (on a large array)
#include <stdlib.h>
#include "any.h"
#define N 2147484646

int main() {
  unsigned int i;
  int n, k, v;
  int *t = calloc(N,sizeof(int));
  if (t==NULL) {
    // no enough memory
    return -1;
  }
  //@ assert \valid(t+(0..2147484646-1));  // We admit this assert that frama-C does not know prove yet !
  /*@ loop invariant i <= N; 
    @ loop invariant \forall unsigned int k; k < i ==> t[k] == k-1000;
    @ loop assigns i, t[0..i-1];
    @*/
  for (i=0; i<N; i++) {
    t[i]=i-1000;
  }
  n = any(); // 1st integer = num of iterations.
  if (n < 0) n=0;
  k = 0;
  /*@ loop invariant k <= n; 
    @ loop assigns k, v, i; */
  while (k < n) {
    k++;
    v = any(); // next integers = value to search
    i = binary_search(t, N, v);
    if (i < N){
      /*@ assert v==i-1000; */
      if (v!=i-1000) return k;
    } else {
      /*@ assert i==N; */
      if (i!=N) return k;
    }
  }
  return 0;
}
