# Program testing and verification (WMM9MO73)

by David Monniaux (DM), Sylvain Boulmé (SB), Radu Iosif (RI) and Lionel Rieg (LR)

**emails** firstname.name@univ-grenoble-alpes.fr

Please clone this repository and keep it synchronized when teachers update it.

## Contents

1. **25 sep** Lecture (DM) - test and properties, undefined behaviors, Valgrind, ASan/UBSan
2. **02 oct** Lecture (DM) - concolic testing
3. **09 oct** Lecture (DM) - undecidability
4. **16 oct** Computer session (DM) - Klee (1/2)
5. **23 oct** Computer session (DM) - Klee (2/2)
6. **06 nov** Computer session (SB) - Tutorial to Frama-C WP plugin (1/2)
    + **Before the session**, please read *Section 1* (6 pages) of the [introduction to Frama-C WP plugin](Tutorial-Frama-C-WP/frama-C-wp-tutorial.pdf)
    + At the beginning of the session, read instructions in *Section 2* and get the [provided files](Tutorial-Frama-C-WP/PROVIDED_FILES/)
7. **13 nov** Computer session (SB) - Tutorial to Frama-C WP plugin (2/2)
    + Continue on the [provided files](Tutorial-Frama-C-WP/PROVIDED_FILES/)
8. **20 nov** Lecture (SB) - introduction to Hoare logic
    + [slides](intro_hoare_logic_handout.pdf) or [compact version in 4up](intro_hoare_logic_handout-4up.pdf)
9. **27 nov** Lecture (SB+RI) - From Hoare logic to separation logic.
10. **4 dec** Computer session (LR) (1/2)
    + **Before the session**, please [download](https://github.com/verifast/verifast/releases) and install the VeriFast tool (version 21.04) and make sure you can run the IDE (vfide).
    + For the session, you will need to download two files: the short [verifast-tutorial](tutorial-VeriFast/verifast-tutorial.pdf) and the exercise file [tp-verifast.v](tutorial-VeriFast/tp-verifast.c).
11. **11 dec** Lecture (RI) - separation logic
    + [slides](lecture_seplog.pdf)
    + Additional reading 1: http://www0.cs.ucl.ac.uk/staff/p.ohearn/papers/bi-assertion-lan.pdf
    + Additional reading 2: https://www.cs.cmu.edu/~jcr/seplogic.pdf
    + Course notes on separation logic: http://www0.cs.ucl.ac.uk/staff/p.ohearn/papers/Marktoberdorf11LectureNotes.pdf
    + Older course on separation logic: https://www.cs.cmu.edu/afs/cs.cmu.edu/project/fox-19/member/jcr/www15818As2011/cs818A3-11.html
    + Course notes on classical Hoare logic: http://web.cecs.pdx.edu/~apt/cs558/Gordon.pdf
12. **18 dec** Computer session (LR) (2/2)  
    Uses the same exercise file as the first practice session.

## Mandatory Frama-C Homework (part of WMM9MO73 ECTS)


Complete the following files of the *Frama-C Tutorial*: [`div1.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/div1.c),
[`linear_search.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/linear_search.c), [`array_copy.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/array_copy.c),
 [`min_sort.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/min_sort.c) and [`binary_search.c`](Tutorial-Frama-C-WP/PROVIDED_FILES/binary_search.c).

### How to submit ###

Please, respect the submission format (see below).
If you have an Ensimag account, please submit your archive on [the dedicated Teide website](https://teide.ensimag.fr/interface_view_project.php?prjid=3372).
Otherwise, simply email your archive to Sylvain.Boulme@imag.fr?subject=[Frama-C Submission].

**Deadline: 1 décembre 2023**

### Format of the submission ###

Your submission should consists of a `.tar.gz` or a `.zip` archive containing your completed `*.c`. 
Your archive may also contain a `README` or `README.md` pointing the issues or the features of your solutions.

Your submission should contain *at most* one **incomplete** solution. 
This simply means that if you already have an unfinished file, then you should not start to work on another file.

A solution is complete, iff all WP Goals are marked `Valid` by `frama-c -wp -wp-rte -wp-split` with two exceptions (i.e. the following goals are marked as `Timeout`):

  * `buggy_call_div_requires` in `div1.c` (this is a test that `frama-c` rejects incorrect aliases).
  * `typed_main_assert` in `binary_search.c` (this is due to incomplete support of `calloc` by `frama-c`).

Feel free to email Sylvain.Boulme@imag.fr?subject=[Frama-C Question] in case of trouble with Frama-C or with this homework.
