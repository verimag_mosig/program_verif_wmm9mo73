# Setting up paths
We have installed Klee and other software that it uses on the ENSIMAG machines. Installing them on your own machine may be nontrivial, and we will not provide support for using a personal installation.

After logging in, you need to set up various paths for using that software (this must be done after every login; you may wish to alter your startup scripts to make it automatic):
```
source /matieres/WMM9MO73/klee.sh
```

Most notably, the `MATIERE` environment variable is set to point to that directory.

# First examples
In the `examples` directory you will find some sample files.

## Hello, world
We need to use a specially set up LLVM compiler to obtain bitcode files compatible suitable for Klee in addition to executable code for the processor. For instance, let us compile `hello_world.c` to `hello_world`:

```
wllvm -g hello_world.c -o hello_world
```

This command takes the same options as `clang` and can similarly be used to compile and link programs. Here, we specified `-g` to get debugging information.

The `hello_world` file contains both executable code (which can be run using `./hello_world`) and bitcode, which can be extracted using

```
extract-bc hello_world
```

A `hello_world.bc` file is created. This file can be executed by the LLVM bitcode interpreter (a normal interpreter for concrete execution):

```
 lli hello_world.bc
Hello, world, 0 times.
Hello, world, 1 times.
Hello, world, 2 times.
Hello, world, 3 times.
Hello, world, 4 times.
Hello, world, 5 times.
Hello, world, 6 times.
Hello, world, 7 times.
Hello, world, 8 times.
Hello, world, 9 times.
```

We can also run it using Klee:
```
$ klee hello_world.bc
KLEE: output directory is "/user/2/monniaud/program_verif_wmm9mo73/Klee/examples/klee-out-0"
KLEE: Using STP solver backend
KLEE: WARNING: undefined reference to function: printf
KLEE: WARNING ONCE: calling external: printf(94170041167456, 0) at hello_world.c:6 3
Hello, world, 0 times.
Hello, world, 1 times.
Hello, world, 2 times.
Hello, world, 3 times.
Hello, world, 4 times.
Hello, world, 5 times.
Hello, world, 6 times.
Hello, world, 7 times.
Hello, world, 8 times.
Hello, world, 9 times.

KLEE: done: total instructions = 110
KLEE: done: completed paths = 1
KLEE: done: partially completed paths = 0
KLEE: done: generated tests = 1
```

Let us comment. Klee says it executed one single complete path in the code, which is unsurprising since this program is fully deterministic and has a single valid execution (print 10 lines of hello messages and exit). It warns us that we called `printf()` as an external function; if the function had had symbolic arguments, it would have had to make them concrete. It uses STP as a solver backend, but here there was not much to solve since there were no symbolic variables.

Note that Klee created a directory `klee-out-0` and a symbolic link to it, `klee-last`. These contain files with symbolic execution path details and associated concrete traces.

## First symbolic example
The `first_symbolic.c` contains calls for symbolic variables:
```
#include <stdio.h>
#include <klee/klee.h>

int main() {
  int x;
  klee_make_symbolic(&x, sizeof(x), "x");
  if (x < 0) {
    printf("negative\n");
  }
  if (x > 1000) {
    printf("large\n");
  }
  return 0;
}
```

The `klee_make_symbolic(ptr, size, name)` call erases the data pointed to by `ptr` of size `size`, replace it by a symbolic value, and give the symbolic value the name `name` in the log files (the name does not have to be related to the name of the variable, though this has obvious mnemotechnic interest).

Let us compile this code:
```
wllvm -g -lkleeRuntest first_symbolic.c -o first_symbolic
extract-bc first_symbolic
```

(The option `-lkleeRuntest` placates the linker. `wllvm` produces both executable code and bitcode, and for the executable code it needs an executable function `klee_make_symbolic()`, which is provided by the `kleeRuntest' library. This function may be used to replay test cases into the actual executable; we'll see that later.)

Let us run Klee:
```
 klee first_symbolic.bc
KLEE: output directory is "/user/2/monniaud/program_verif_wmm9mo73/Klee/examples/klee-out-0"
KLEE: Using STP solver backend
KLEE: WARNING: undefined reference to function: printf
KLEE: WARNING ONCE: calling external: printf(93879632818016) at first_symbolic.c:9 3
negative
large

KLEE: done: total instructions = 22
KLEE: done: completed paths = 3
KLEE: done: partially completed paths = 0
KLEE: done: generated tests = 3
```

Klee unsurprisingly explored 3 symbolic paths: x < 1000, 0 ≤ x ≤ 1000, x > 1000. Note that it still performed the calls to `printf()` on each path; in general the output will be garbled.

Let us see which values it found:
```
$ ktest-tool klee-last/test000001.ktest 
ktest file : 'klee-last/test000001.ktest'
args       : ['first_symbolic.bc']
num objects: 1
object 0: name: 'x'
object 0: size: 4
object 0: data: b'\x00\x00\x00\x80'
object 0: hex : 0x00000080
object 0: int : -2147483648
object 0: uint: 2147483648
object 0: text: ....
```

Thus one path can be exercised with x = -2147483648. Likewise, `test000002.ktest` is exercised with x = 0, and `test000003.ktest` is exercised with x = 1001.

## Many symbolic variables
The example `many_symbolic.c` creates many symbolic variables, to which it gives names for the log file. `klee_range(low, high, name)` yields an integer value from `low` included to `high` excluded, and gives it the name `name`.

```
#include <stdio.h>
#include <assert.h>
#include <klee/klee.h>

#define N 10
#define MAX 20

int main() {
  int t[N], sum = 0;
  char name[20];
  for(int i=0; i<N; i++) {
    snprintf(name, sizeof(name), "t[%d]", i);
    t[i] = klee_range(0, MAX, name);
    sum += t[i];
  }
  klee_assert(sum < MAX * N);
  return 0;
}
```

Compile and run Klee on it. Klee sees one path and outputs no warning: the assertion is correct! 

(Note: if you are using `klee_assert`, you need to `#include` both `assert.h` and `klee.h`, otherwise you get strange warnings such as `implicit declaration of function '__assert_fail' is invalid in C99`.)

Let us now modify the assertion into `sum < (MAX-1) * N`. We obtain
```
$ klee many_symbolic.bc
KLEE: output directory is "/user/2/monniaud/program_verif_wmm9mo73/Klee/examples/klee-out-6"
KLEE: Using STP solver backend
KLEE: WARNING: undefined reference to function: snprintf
KLEE: WARNING ONCE: calling external: snprintf(93890714264800, 20, 93890714202240, 0) at many_symbolic.c:13 31
KLEE: ERROR: many_symbolic.c:16: ASSERTION FAIL: sum < (MAX-1) * N
KLEE: NOTE: now ignoring this error at this location

KLEE: done: total instructions = 652
KLEE: done: completed paths = 1
KLEE: done: partially completed paths = 1
KLEE: done: generated tests = 2
```

We see in directory `klee-last` some files
```
test000001.ktest
test000002.assert.err
test000002.kquery 
test000002.ktest
```
including one pointing to an assertion failure:
```
$ cat klee-last/test000002.assert.err 
Error: ASSERTION FAIL: sum < (MAX-1) * N
File: many_symbolic.c
Line: 16
assembly.ll line: 69
State: 2
Stack: 
	#000000069 in main () at many_symbolic.c:16
```

Let us see how we got to the assertion failure:
```
$ ktest-tool klee-last/test000002.ktest |egrep 'name:|int :'
object 0: name: 't[0]'
object 0: int : 19
object 1: name: 't[1]'
object 1: int : 19
object 2: name: 't[2]'
object 2: int : 19
object 3: name: 't[3]'
object 3: int : 19
object 4: name: 't[4]'
object 4: int : 19
object 5: name: 't[5]'
object 5: int : 19
object 6: name: 't[6]'
object 6: int : 19
object 7: name: 't[7]'
object 7: int : 19
object 8: name: 't[8]'
object 8: int : 19
object 9: name: 't[9]'
object 9: int : 19
```

## Searching in an array
The procedure in `find_in_array.c` is buggy.

We wrote this example so that it can be run both as a normal program and run through Klee using symbolic execution; this is decided according to the `KLEE` preprocessor definition. Thus, when using `wllvm` to compile it, you need to pass the `-DKLEE` option.

Find the bug using Klee.

Write a procedure that checks that the array is sorted in ascending order and run the linear find only in this case. Find the bug again.

Fix the bug.
	
# File reading example
Consider the following program, in directory `file_read`, which reads numbers from standard input and prints them along with a short prefix on standard output:
```
#include <stdio.h>
#include <stdlib.h>

int main() {
  FILE *fp = stdin;
  while(! feof(fp)) {
    int dat;
    if (fscanf(fp, "%d\n", &dat) != 1) {
      exit(1);
    }
    char out[13];
    sprintf(out, "data = %d", dat);
    puts(out);
  }
  return 0;
}
```

This program can be run on sample data:
```
$ ./file_read  <input.dat
data = 45
data = 1223
data = 12
data = 78
data = 93
data = 11109
```

Yet this program is incorrect. Can you spot the problem?

There are ways to get Klee to consider that an input file is symbolic but they are cumbersome to use. If we are interested in investigating internal computations, the simplest is to replace input functions by symbolic functions (and disable outputs—more speed, less clutter). In order to be able to use the same source code for normal program operations and symbolic execution, we can use conditional compilation according to the `KLEE` preprocessor definition. The `read_int()` function, like `fscanf()`, can either read an integer, or signal that there is no integer to read.
```
#ifdef KLEE
int read_int(FILE *fp, int *dat, const char *id) {
  if (klee_range(0, 2, "read_int_available")) {
    klee_make_symbolic(dat, sizeof(*dat), id);
    return 1;
  } else {
    return 0;
  }
}
#else
int read_int(FILE *fp, int *dat, const char *id) {
  return fscanf(fp, "%d\n", dat);
}
#endif

int main() {
  FILE *fp = stdin;
  while(! feof(fp)) {
    int dat;
    if (read_int(fp, &dat, "dat") != 1) exit(1);
    char out[13];
    sprintf(out, "data = %d", dat);
#ifndef KLEE
    puts(out);
#endif
  }
  return 0;
}
```

Compile this program with the preprocessor macro `KLEE` defined:
```
wllvm -lkleeRuntest -g -DKLEE file_read_modified.c -o file_read_modified
extract-bc file_read_modified
```

We are checking for errors in a program that calls a library function (`sprintf()`). By default, Klee executes library functions as external calls, which can't happen with symbolic arguments. It is possible to get Klee to use a special C library compiled to LLVM bitcode by passing the option `-libc=uclibc`. Option `-posix-runtime` tells Klee to provide certain system calls that the C library is likely to make. Finally, option `--exit-on-error` tells Klee to stop executing once it finds a runtime error.

Run Klee with those options, look at the `.err` file in `klee-last`, use `ktest-tool` on the associated `.ktest` file. Do you now understand the bug?

# Try on your own programs
